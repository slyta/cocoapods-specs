Pod::Spec.new do |s|
  s.name     = 'GPUImage@FP'
  s.version  = '0.1.2'
  s.license  = 'BSD'
  s.platform = :ios
  s.summary  = 'Versioned fork of GPUImage.'
  s.description  = 'Versioned fork of GPUImage. OpenGL ES 2-based image and real-time camera filters for iOS.'
  s.homepage = 'https://github.com/BradLarson/GPUImage'
  s.author   = { 'Brad Larson' => 'contact@sunsetlakesoftware.com' }
  s.source   = { :git => 'https://slyta@bitbucket.org/slyta/gpuimage-fork.git', :tag => '0.1.2' }
  s.source_files = 'framework/Source/**/*.{h,m}'
  s.osx.exclude_files = 'framework/Source/iOS/**/*.{h,m}'
  s.ios.exclude_files = 'framework/Source/Mac/**/*.{h,m}'
  s.frameworks   = ['OpenGLES', 'CoreVideo', 'CoreMedia', 'QuartzCore', 'AVFoundation']

  s.requires_arc = true
end

