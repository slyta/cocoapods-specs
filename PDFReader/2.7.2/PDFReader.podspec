Pod::Spec.new do |s|
  s.name     = 'PDFReader'
  s.version  = '2.7.2'
  s.license  = 'MIT'
  s.platform = :ios
  s.summary  = 'PDF Reader Core for iOS'
  s.description  = 'Open source PDF reader code for fellow iOS developers struggling with wrangling PDF files onto iOS device screens.'
  s.homepage = 'https://github.com/vfr/Reader'
  s.author   = { 'Julius Oklamcak' => 'joklamcak@gmail.com' }
  s.source   = { :git => 'https://github.com/vfr/Reader.git', :commit => '7aad0529e0410cb554bdc3c568dbeb09cb49d864' }
  s.source_files = 'Sources/**/*.{h,m}'
  s.frameworks   = ['UIKit', 'Foundation', 'CoreGraphics', 'QuartzCore', 'ImageIO', 'MessageUI']

  s.requires_arc = true
end
